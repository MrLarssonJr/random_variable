#![warn(missing_docs)]

//! Data structures for playing around with frequency distributions for random variables.
//!
//! This crate aims to make it simple to create a random variable with some probability
//! distribution and then play around with it how the distribution behaves through different
//! operations.

use std::collections::HashMap;
use std::ops::{Add, Mul, Div};
use std::error::Error;
use std::fmt::{Display, Formatter, Debug};
use std::hash::{Hash, Hasher};
use num::{Zero, One};
use num::rational::Rational64;
use std::convert::TryFrom;

/// An enum for communicating that an error occurred while constructing a
/// [DiscreteRandomVariable](struct.DiscreteRandomVariable.html).
#[derive(Debug)]
pub enum ConstructionError<V> {
    /// The data constructing the discrete random variable contains duplicate values.
    /// Each value should only map to one frequency.
    DuplicateValue(V),

    /// The frequency given for the values does not sum to 1.
    ///
    /// All possible values must be given with corresponding frequency.
    NotSumToOne(Rational64),
}

impl<V> Display for ConstructionError<V>
where
    V: Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use ConstructionError::*;
        let msg = match self {
            DuplicateValue(val) =>
                format!("{} is a not unique in the given values.", val),
            NotSumToOne(sum) =>
                format!("The sum of the given frequencies are {} (it should be 1).", sum)
        };

        write!(f, "{}", msg)
    }
}

impl<V: Debug + Display> Error for ConstructionError<V> {}

/// A struct for holding information related to the probability
/// for a certain value in a [DiscreteRandomVariable](struct.DiscreteRandomVariable.html).
#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
struct Probability {
    description: Option<&'static str>,
    value: Rational64,
}

/// A discrete random variable.
///
/// A discrete random variable describes a mapping from a random process to
/// a discrete set of values. This implementation just describes the frequency
/// distribution of that mapping, i.e. the probability for each possible value
/// of the mapping.
#[derive(Debug)]
pub struct DiscreteRandomVariable<V> {
    /// The random variables [frequency distribution](https://en.wikipedia.org/wiki/Frequency_distribution).
    fd: HashMap<V, Probability>,
}

impl<V> Clone for DiscreteRandomVariable<V>
where
    V: Clone,
{
    fn clone(&self) -> Self {
        DiscreteRandomVariable {
            fd: self.fd.clone()
        }
    }
}

impl<V> PartialEq for DiscreteRandomVariable<V>
where
    V: Eq + Hash,
{
    fn eq(&self, other: &Self) -> bool {
        for (val, &p1) in self.fd.iter() {
            let p2 = if let Some(&freq) = other.fd.get(val) {
                freq
            } else {
                return false;
            };

            // Weird comparison to comply with hash implementation
            if p1 != p2 {
                return false;
            }
        }

        return true;
    }
}

impl<V> Eq for DiscreteRandomVariable<V>
where V: Eq + Hash,
{}

impl<V> Hash for DiscreteRandomVariable<V>
where
    V: Hash,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        for (val, &p) in self.fd.iter() {
            val.hash(state);
            p.hash(state);
        }
    }
}

impl<V> TryFrom<Vec<(V, Rational64)>> for DiscreteRandomVariable<V>
where
    V: Hash + Eq,
{
    type Error = ConstructionError<V>;

    fn try_from(values: Vec<(V, Rational64)>) -> Result<Self, Self::Error> {
        use ConstructionError::*;

        let mut freq_sum = Rational64::zero();
        let mut fd = HashMap::new();

        for (val, freq) in values {
            if fd.contains_key(&val) {
                return Err(DuplicateValue(val));
            }

            freq_sum += freq;

            let p = Probability {
                description: None,
                value: freq,
            };

            fd.insert(val, p);
        }

        if freq_sum != Rational64::one() {
            return Err(NotSumToOne(freq_sum))
        }

        Ok(
            DiscreteRandomVariable {
                fd,
            }
        )
    }
}

impl<V> TryFrom<Vec<(V, Rational64, &'static str)>> for DiscreteRandomVariable<V>
    where
        V: Hash + Eq,
{
    type Error = ConstructionError<V>;

    fn try_from(values: Vec<(V, Rational64, &'static str)>) -> Result<Self, Self::Error> {
        use ConstructionError::*;

        let mut freq_sum = Rational64::zero();
        let mut fd = HashMap::new();

        for (val, freq, desc) in values {
            if fd.contains_key(&val) {
                return Err(DuplicateValue(val));
            }

            freq_sum += freq;

            let p = Probability {
                description: Some(desc),
                value: freq,
            };

            fd.insert(val, p);
        }

        if freq_sum != Rational64::one() {
            return Err(NotSumToOne(freq_sum))
        }

        Ok(
            DiscreteRandomVariable {
                fd,
            }
        )
    }
}

impl<V> DiscreteRandomVariable<V>
where
    V: Hash + Eq,
{
    /// Create a new discrete random variable by mapping the possible values
    /// of this one to new possible values.
    ///
    /// The probability for all values mapped to the same new value will be summed
    /// to produce the probability for the new value.
    pub fn map<W>(&self, mut f: impl FnMut(&V) -> W) -> DiscreteRandomVariable<W>
    where
        W: Hash + Eq,
    {
        let mut new_fd = HashMap::new();

        for (val, &Probability{value: freq, ..}) in self.fd.iter() {
            let new_val = f(val);
            new_fd.entry(new_val)
                .and_modify(|Probability{value: pending_freq, ..}| *pending_freq += freq)
                .or_insert(Probability {
                    description: None,
                    value: freq,
                });
        }

        DiscreteRandomVariable {
            fd: new_fd,
        }
    }

    /// Get the probability for a certain value.
    ///
    /// If queried with a value that is not a possible value for this random
    /// variable, then zero will be returned.
    pub fn get_probability(&self, evidence: &V) -> Rational64 {
        self.fd.get(evidence)
            .map(|&p| p.value)
            .unwrap_or(Rational64::zero())
    }
}

/// A random variable may be interpreted as model or a hypothesis of how a system works.
/// Random variables may themselves be used as the possible values for a discrete random
/// variable. Such a meta random variable may be viewed as a set of hypothesis with a
/// probability assigned to each hypothesis.
type HypothesisSet<V> = DiscreteRandomVariable<DiscreteRandomVariable<V>>;

impl<V> HypothesisSet<V>
where
    V: Hash + Eq + Clone,
{
    /// Perform [Bayesian inference](https://en.wikipedia.org/wiki/Bayesian_inference) on a set of
    /// hypothesises.
    pub fn infer(&self, evidence: &V) -> HypothesisSet<V> {
        let mut new_fd = HashMap::new();

        for (hypothesis, &Probability{ value: prior, description: desc}) in self.fd.iter() {
            let likelihood = hypothesis.get_probability(evidence);
            let conjugate_prior = Rational64::one() - prior;
            let conjugate_likelihood: Rational64 = self.fd.iter()
                .filter(|(& ref h, &_)| h != hypothesis)
                .map(|(h, &Probability{value: freq, ..})| (freq / conjugate_prior) * h.get_probability(evidence))
                .sum();

            let posterior = (prior * likelihood) / (prior * likelihood + conjugate_prior * conjugate_likelihood);
            let cloned: DiscreteRandomVariable<V> = hypothesis.clone();

            new_fd.insert(cloned, Probability { value: posterior, description: desc });
        }

        DiscreteRandomVariable {
            fd: new_fd
        }
    }
}

impl<V> Display for DiscreteRandomVariable<V>
where
    V: Display
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for (index, (val, p)) in self.fd.iter().enumerate() {
            if let Some(desc) = p.description {
                write!(f, "{val} - {freq} - {desc}", val=val, freq=p.value, desc=desc)?;
            } else {
                write!(f, "{val} - {freq}", val=val, freq=p.value)?;
            }

            if index < self.fd.len() - 1 {
                write!(f, "\n")?;
            }
        }

        Ok(())
    }
}

impl<'a, 'b, V> Add<&'b DiscreteRandomVariable<V>> for &'a DiscreteRandomVariable<V>
where
    V: Hash + Eq,
    &'a V: Add<&'b V, Output=V>,
{
    type Output = DiscreteRandomVariable<V>;

    fn add(self, rhs: &'b DiscreteRandomVariable<V>) -> Self::Output {
        let mut new_fd = HashMap::new();

        for (val1, &Probability{value: freq1, ..}) in self.fd.iter() {
            for (val2, &Probability{value: freq2, ..}) in rhs.fd.iter() {
                let new_val = val1 + val2;
                let new_freq = freq1 * freq2;

                new_fd
                    .entry(new_val)
                    .and_modify(|Probability{value: partial_freq, ..}| *partial_freq += new_freq)
                    .or_insert(Probability {
                        value: new_freq,
                        description: None,
                    });
            }
        }

        DiscreteRandomVariable {
            fd: new_fd
        }
    }
}

impl<'a, V> Mul<i64> for &'a DiscreteRandomVariable<V>
where
    V: Zero + Hash + Eq,
    for <'b> &'a V: Add<&'b V, Output=V>,
{
    type Output = DiscreteRandomVariable<V>;

    fn mul(self, rhs: i64) -> Self::Output {
        let mut res =
            if let Ok(res) = DiscreteRandomVariable::try_from(vec![(V::zero(), Rational64::one())]) {
                res
            } else {
                //Should never been reached if the constant defined here is well defined.
                unreachable!();
            };

        for _ in 0..rhs {
            let partial = self + &res;
            res = partial;
        }

        res
    }
}

impl<'a, V> Div<i64> for &'a DiscreteRandomVariable<V>
where
    V: Hash + Eq,
    &'a V: Div<i64, Output=V>,
{
    type Output = DiscreteRandomVariable<V>;

    fn div(self, rhs: i64) -> Self::Output {
        let mut new_fd = HashMap::new();

        for (val, &freq) in self.fd.iter() {
            new_fd.insert(val / rhs, freq);
        }

        DiscreteRandomVariable {
            fd: new_fd,
        }
    }
}
