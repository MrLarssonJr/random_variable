use random_variable::DiscreteRandomVariable;
use num::rational::Rational64;
use std::convert::TryFrom;

fn main() {
    let possible_values: Vec<(Rational64, Rational64)> = (0..100)
        .map(|index| (
            Rational64::new((100 + index) as i64, 100),
            Rational64::new(1, 100),
        ))
        .collect();

    let single_char: DiscreteRandomVariable<Rational64> = DiscreteRandomVariable::try_from(possible_values).unwrap();

    let models = vec![
        ((&single_char * 1).map(|&val| val.round()), Rational64::new(1, 4), "Single char"),
        ((&single_char * 2).map(|&val| val.round()), Rational64::new(1, 4), "Two chars"),
        ((&single_char * 3).map(|&val| val.round()), Rational64::new(1, 4), "Three chars"),
        ((&single_char * 4).map(|&val| val.round()), Rational64::new(1, 4), "Four chars"),
    ];

    println!("Probability distributions:");
    for (model, _, desc) in models.iter() {
        println!("{}", desc);
        println!("{}\n", model);
    }

    let priors: DiscreteRandomVariable<DiscreteRandomVariable<Rational64>> = DiscreteRandomVariable::try_from(models).unwrap();
    println!("Prior probabilites for models:\n{}", priors);
    let posteriors = priors.infer(&Rational64::from_integer(4));
    println!("Posterior probabilites for models:\n{}", posteriors);

}